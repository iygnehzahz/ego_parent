package com.hr.dubbo.service.impl;

import com.hr.dubbo.service.TbItemCatDubboService;
import com.hr.mapper.TbItemCatMapper;
import com.hr.pojo.TbItemCat;
import com.hr.pojo.TbItemCatExample;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class TbItemCatDubboServiceImpl implements TbItemCatDubboService {
    @Autowired
    private TbItemCatMapper tbItemCatMapper;
    @Override
    public List<TbItemCat> selectByPid(Long pid) {
        TbItemCatExample example = new TbItemCatExample();
        example.createCriteria().andParentIdEqualTo(pid);
        return tbItemCatMapper.selectByExample(example);
    }

    @Override
    public TbItemCat selectByid(Long id) {
        return tbItemCatMapper.selectByPrimaryKey(id);
    }
}
