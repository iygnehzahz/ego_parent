package com.hr.dubbo.service.impl;


import com.hr.dubbo.service.TbItemParamItemDubboService;
import com.hr.mapper.TbItemParamItemMapper;
import com.hr.pojo.TbItemParamItem;
import com.hr.pojo.TbItemParamItemExample;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class TbItemParamItemDubboServiceImpl implements TbItemParamItemDubboService {
    @Autowired
    private TbItemParamItemMapper tbItemParamItemMapper;
    @Override
    public TbItemParamItem selectByItemId(Long itemId) {
        TbItemParamItemExample example = new TbItemParamItemExample();
        example.createCriteria().andItemIdEqualTo(itemId);
        List<TbItemParamItem> list = tbItemParamItemMapper.selectByExampleWithBLOBs(example);
        if(list!=null&&list.size()==1){
            return list.get(0);
        }
        return null;
    }
}
