package com.hr.item.service;


import com.hr.item.pojo.ItemCategoryNav;

public interface ItemService {
    /**
     * 显示导航菜单
     * @return
     */
    ItemCategoryNav showItemCat();
}
