package com.hr.service.impl;

import com.hr.commons.exception.DaoException;
import com.hr.commons.pojo.EasyUIDatagrid;
import com.hr.commons.pojo.EgoResult;
import com.hr.commons.util.IDUtils;
import com.hr.dubbo.service.TbContentDubboService;
import com.hr.pojo.TbContent;
import com.hr.service.TbContentService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TbContentServiceImpl implements TbContentService {
    @Reference
    private TbContentDubboService tbContentDubboService;
    @Override
    public EasyUIDatagrid showContent(Long categoryId, int page, int rows) {
        List<TbContent> list = tbContentDubboService.selectBypage(categoryId, page, rows);
        long total = tbContentDubboService.selectCountByCategoryid(categoryId);
        return new EasyUIDatagrid(list,total);
    }

    @Override
    public EgoResult insert(TbContent tbContent) {
        tbContent.setId(IDUtils.genItemId());
        Date date = new Date();
        tbContent.setCreated(date);
        tbContent.setUpdated(date);
        int index = tbContentDubboService.insert(tbContent);
        if(index==1){
            return EgoResult.ok();
        }
        return EgoResult.error("新增失败");
    }

    @Override
    public EgoResult update(TbContent tbContent) {
        tbContent.setUpdated(new Date());
        int index = tbContentDubboService.update(tbContent);
        if(index==1){
            return EgoResult.ok();
        }
        return EgoResult.error("修改失败");
    }

    @Override
    public EgoResult delete(long[] ids) {
        try {
            int index = tbContentDubboService.deleteByIds(ids);
            if(index==1){
                return EgoResult.ok();
            }
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return EgoResult.error("删除失败");
    }
}
