package com.hr.service.impl;

import com.hr.commons.pojo.EgoResult;
import com.hr.dubbo.service.TbItemParamItemDubboService;
import com.hr.pojo.TbItemParamItem;
import com.hr.service.TbItemParamItemService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

@Service
public class TbItemParamItemServiceImpl implements TbItemParamItemService {
    @Reference
    private TbItemParamItemDubboService tbItemParamItemDubboService;
    @Override
    public EgoResult showItemParamItem(Long itemId) {
        TbItemParamItem tbItemParamItem = tbItemParamItemDubboService.selectByItemId(itemId);
        if(tbItemParamItem!=null){
            return EgoResult.ok(tbItemParamItem);
        }
        return EgoResult.error("查询失败");
    }
}
