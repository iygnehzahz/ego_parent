package com.hr.service;


import com.hr.commons.pojo.EasyUITree;
import com.hr.commons.pojo.EgoResult;
import com.hr.pojo.TbContentCategory;

import java.util.List;

public interface TbContentCategoryService {
    /**
     * 显示内容分类树状菜单
     * @param pid 父id
     * @return easyui tree要的数据
     */
    List<EasyUITree> showContentCategory(Long pid);

    /**
     * 新增
     * @param tbContentCategory
     * @return
     */
    EgoResult insert(TbContentCategory tbContentCategory);

    /**
     * 修改分类名称
     * @param tbContentCategory
     * @return
     */
    EgoResult update(TbContentCategory tbContentCategory);

    /**
     * 删除分类
     * @param id
     * @return
     */
    EgoResult delete(Long id);
}
