package com.hr.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 周恒哲
 * @date 2020/05/12
 * 大广告位
 * var data = [{"srcB":"http://image.ego.com/images/2015/03/03/2015030304360302109345.jpg",
 * "height":240,"alt":"","width":670,"src":"http://image.ego.com/images/2015/03/03/2015030304360302109345.jpg","widthB":550,
 *  "href":"http://sale.jd.com/act/e0FMkuDhJz35CNt.html?cpdad=1DLSUE","heightB":240},
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BigAd {

    //备份图片
    private String srcB;
    //显示图片的高
    private int height;
    //鼠标悬浮提示信息
    private String alt;
    //显示图片的宽
    private int width;
    //显示图片的url
    private String src;
    //备份图片宽
    private int widthB;
    //点击图片跳转路径
    private String href;
    //备份图片的高
    private int heightB;


}
