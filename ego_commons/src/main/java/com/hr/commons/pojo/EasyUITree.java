package com.hr.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * EasyUI Tree
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EasyUITree {
    private Long id;
    private String text;
    private String state;

}
