package com.hr.commons.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * EasyUI datagrid数据模板
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class EasyUIDatagrid {
    private List<?> rows;
    private long total;
}
