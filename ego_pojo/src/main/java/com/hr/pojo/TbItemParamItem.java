package com.hr.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhz
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TbItemParamItem implements Serializable {
    private Long id;

    private Long itemId;

    private Date created;

    private Date updated;

    private String paramData;
}