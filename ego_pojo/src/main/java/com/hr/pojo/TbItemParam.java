package com.hr.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhz
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TbItemParam implements Serializable {
    private Long id;

    private Long itemCatId;

    private Date created;

    private Date updated;

    private String paramData;
}