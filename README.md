# ego_parent

#### 介绍
本项目的技术选型为：
JSP
jQuery
EasyUI
Spring Boot
Spring MVC
MyBatis
MyBatis Generator
Druid
Logback
MyBatis PageHelper
Dubbo
FastDFS-java-client
Spring Data for Apache Solr
Spring AMQP
Spring Data Redis
Spring Security
HttpClient/RestTemplate
开发工具和环境为：
Maven
Linux
IDEA
Zookeeper
FastDFS
Nginx
Solr
RabbitMQ
Redis
MyCat
MySQL
Tomcat
功能模块划分：
ego_parent: 父项目（maven 项目使用下划线，不要使用中划线）
ego_pojo: 实体类。
ego_api : 编写接口。
ego_mapper: 编写所有数据库访问代码。
ego_provider: 编写接口实现。
ego_commons: 所有公共资源。工具类、公用实体类。
ego_manage:整个网站后台，由于功能完成较少，所以没有过多拆分项目。
ego_portal: 门户。
ego_search: solr 搜索。
ego_item: 商品详情。
ego_cart:购物车。
ego_passport:单点登录系统
ego_trade: 订单系统
ego_redis: 包含了所有对 Redis 操作。只要依赖这个项目就可以操作 redis。为了重
用。
ego_rabbitmq_sender: 专门完成对 rabbitmq 发送消息。依赖此项目即可发送消息。
ego_rabbitmq_receive: 监听 rabbitmq 队列。

#### 软件架构
软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/172732_bb862014_2241181.png "屏幕截图.png")

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
